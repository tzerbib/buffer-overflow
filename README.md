# Buffer overflows  

This repository gives many buffer overflows examples.  
It gathers the notes that I took during my different tests, mostly in French and
it was only done for my own comprehension.  
It aims to explain how to make a buffer overflow exploit and present most
of the problems I encoutered when I tried to do them.  
It includes some scripts found on the Internet, sometimes modified/commented.  


## Licence  

This project is under MIT Licence.  
You could find a copy of the MIT License in [LICENCE](LICENCE).